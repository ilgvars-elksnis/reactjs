import React from "react";
import "./styles.css";

import ReactPlayer from "react-player";

export default function App() {
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <h2> Helsreach Grimaldus speach </h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=a8_MjeU8MLM" />
      <h2>ReactJS</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=Ke90Tje7VS0" />
      <h2>Battlestar Galactica</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=q9gt2_SvYPw" />
      <h2>Astartes</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=O7hgjuFfn3A" />
      <h2>C#</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=gfkTfcpWqAY" />
      <h2>Java</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=RRubcjpTkks" />
      <h2>REST API</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=7YcW25PHnAA" />
      <h2>Python</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=Z1Yd7upQsXY" />
      <h2>NodeJS</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=ENrzD9HAZK4" />
      <h2>Shell</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=hwrnmQumtPw" />
      <h2>Bash</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=oxuRxtrO2Ag" />
      <h2>Angular</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=Fdf5aTYRW0E" />
      <h2>Angular</h2>
      <ReactPlayer controls url="https://www.youtube.com/watch?v=Fdf5aTYRW0E" />
    </div>
  );
}
